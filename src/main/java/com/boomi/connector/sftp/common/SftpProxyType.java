//Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.sftp.common;

/**
 * The Enum SftpProxyType.
 * @author sweta.b.das
 */
public enum SftpProxyType {
	    
    	/** The atom. */
    	ATOM,
	    
    	/** The http. */
    	HTTP,
	    
    	/** The socks4. */
    	SOCKS4,
	    
    	/** The socks5. */
    	SOCKS5
}
