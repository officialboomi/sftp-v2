//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

/**
 * The Enum ActionIfFileExists.
 *
 * @author Omesh Deoli
 * 
 *
 */
public enum ActionIfFileExists {

	/** The error. */
	ERROR, /** The force unique names. */
 FORCE_UNIQUE_NAMES, /** The overwrite. */
 OVERWRITE, /** The append. */
 APPEND;
}
